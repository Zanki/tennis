﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulatorTennisGame
{
    public class TennisGame : IGame
    {
        //Limite de juego que hemos elegido
        public int SetLimit { get; set; }
        //Contador de sets que pasan
        public int set { get; set; }
        //Contador de rondas de juegos que pasan
        public int round { get; set; }
        //dibuja tablero de 3 o 5
        public string table { get; set; }
        public string Ganador{get;set;}

        TennisRules tennis = new TennisRules();



        //logica de juego
        public void StartGame()
        {
            Console.WriteLine("***Welcome to Tennis Simulator Game***");
            Console.WriteLine("--------------------------------------");

            this.SetLimit = tennis.SetLimit();
            Console.WriteLine();
            //Creacion de jugadores y asignacion de nombres
            Console.Write("Insert name Player 1: ");
            string GamerOneName = Console.ReadLine();
            Console.Write("Insert name Player 2: ");
            string GamerTwoName = Console.ReadLine();



            Gamer g1 = new Gamer(GamerOneName);
            Gamer g2 = new Gamer(GamerTwoName);
            Console.WriteLine();
            string start = " will start match.";
            //Sorteo inicial de quien iniciara el partido
            if (g1.ThrowBall())
            {
                Console.WriteLine(g1.GamerName + start); 
            }
            else
            {
                Console.WriteLine(g2.GamerName + start);
            }
            
            

            Console.WriteLine("Press key to begin...");
            Console.ReadKey();
            Console.WriteLine("--------------------------------------");
            //Inicializamos ronda y set y la comunicamos antes del bucle
            this.round = 1;
            this.set = 1;
            Console.WriteLine("** Game " + round + " - Set " + set);

            do
            {
               //Bucle termina si ganador es cierto
                bool isWinner = false;
                
                //Comprobamos quien gana punto y lo convertimos a puntuacion adecuada
                string player =tennis.ShootTime(g1, g2);
                tennis.DrawTab(g1, g2, this);

                //Logica de comprobar ganador
                if(tennis.GameWinner(g1, g2))
                {
                    if(tennis.SetWinner(g1, g2))
                    {
                        if(tennis.MatchWinner(g1, g2, this))
                        {//Ganamos todo el partido
                            tennis.CalcGameScore(g1, g2, this);
                            tennis.DrawTab(g1, g2, this);
                            Console.WriteLine("***********************");
                            Console.WriteLine(Ganador.ToUpper() + " WIN THE MATCH!!!          " + table);
                            Console.WriteLine("***********************");
                            isWinner = true;
                            if (isWinner)
                            {
                                break;
                            }
                        }
                        else
                        {//Ganamos el set y el juego
                            tennis.CalcGameScore(g1, g2, this);
                            tennis.ResetGames(g1, g2);
                            this.round = 1;
                            this.set++;
                            tennis.DrawTab(g1, g2, this);
                            Console.WriteLine("***********************");
                            Console.WriteLine(player + " win the game and set!!    " + table);
                            Console.WriteLine();
                            Console.WriteLine("** Game " + round + " - Set " + set);
                        }
                    }
                    else
                    {//Ganamos el un juego
                        tennis.CalcGameScore(g1, g2, this);
                        this.round++;
                        tennis.DrawTab(g1, g2, this);
                        Console.WriteLine("***********************");
                        Console.WriteLine(player + " win the game!             " + table);
                        Console.WriteLine();
                        Console.WriteLine("** Game "+round+" - Set "+set);
                    }
                }
                else
                {//Mostramos el jugador que puntuo
                    tennis.CalcScore(g1, g2);
                    tennis.CalcScore(g2, g1);
                    Console.WriteLine("Point for " + player + "  " + g1.Score + "-" + g2.Score + ")          " + table);
                                      
                }


            } while (true);

            Console.WriteLine();
            Console.WriteLine("Press for exit...");
            Console.ReadKey();
        }
    }
}
