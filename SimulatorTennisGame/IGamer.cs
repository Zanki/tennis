﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulatorTennisGame
{
    interface IGamer
    {
        string GamerName { get; set; }
        string Score { get; set; }
        int Points { get; set; }
        int WinnedGames { get; set; }
        int Set1 { get; set; }
        int Set2 { get; set; }
        int Set3 { get; set; }
        int Set4 { get; set; }
        int Set5 { get; set; }
        int SetCount { get; set; }
         

        bool ThrowBall();
    }
}
