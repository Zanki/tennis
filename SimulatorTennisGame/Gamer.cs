﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimulatorTennisGame
{
    public class Gamer : IGamer
    {
        public Gamer(string gamerName)
        {
            this.GamerName = gamerName;
        }
        //Nombre de jugador, puntos, puntuacion de tenis, juegos ganados, resultado de sets, cantidad de sets ganados, metodo random
        public string GamerName { get; set ; }
        public string Score { get; set; }
        public int Points { get; set; }
        public int WinnedGames { get; set; }
        public int Set1 { get; set; }
        public int Set2 { get; set; }
        public int Set3 { get; set; }
        public int Set4 { get; set; }
        public int Set5 { get; set; }
        public int SetCount { get; set; }

        public bool ThrowBall()
        {
            Random rnd = new Random();
            int number = rnd.Next(1, 3);
            Thread.Sleep(500);
            bool point = number == 1 ? true : false;
            return point;
        }
    }
}
