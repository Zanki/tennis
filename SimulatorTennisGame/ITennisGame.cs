﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulatorTennisGame
{
    interface ITennisGame 
    {
        string ShootTime(Gamer a, Gamer b);
        void ResetPoints(Gamer a, Gamer b);
        void ResetGames(Gamer a, Gamer b);
        void CalcScore(Gamer a, Gamer b);
        bool GameWinner(Gamer a, Gamer b);
        bool SetWinner(Gamer b, Gamer a);
        bool MatchWinner(Gamer a,Gamer b, TennisGame ng);
        void CalcGameScore(Gamer a, Gamer b, TennisGame ng);
        void DrawTab(Gamer a, Gamer b, TennisGame ng);
        int SetLimit();
    }
}
