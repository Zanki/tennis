﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimulatorTennisGame
{
    public class TennisRules
    {
        //Metodo para asignar puntuacion de tenis a puntos obtenidos para cada jugador
        public void CalcScore(Gamer a, Gamer b)
        {
            switch (a.Points)
            {
                case 0:
                    a.Score = "0";
                    break;
                case 1:
                    a.Score = "15";
                    break;
                case 2:
                    a.Score = "30";
                    break;
                case 3:
                    a.Score = "40";
                    break;

            }

            if (a.Points > 3 && a.Points - b.Points == 1)
            {
                a.Score = "AD";
            }
            else if (a.Points > 3 && a.Points == b.Points)
            {
                b.Score = "40";
            }
        }
        //Determina si jugador gana el juego
        public bool GameWinner(Gamer a, Gamer b)
        {
            bool isGameWinner = false;

            if (a.Points > 3 && a.Points >= b.Points + 2)
            {
                isGameWinner = true;            
                a.WinnedGames++;

            }else if(b.Points >3 && b.Points >=a.Points + 2)
            {      
                isGameWinner = true;  
                b.WinnedGames++;
            }
            return isGameWinner;
        }
        //Determina si jugador gana el set
        public bool SetWinner(Gamer b, Gamer a)
        {
            bool isSetWinner = false;

            if (a.WinnedGames >= 6 && a.WinnedGames >= b.WinnedGames + 2)
            {
                isSetWinner = true;
                a.SetCount++;
            }
            else if(b.WinnedGames >= 6 && b.WinnedGames >= a.WinnedGames + 2)
            {
                isSetWinner = true;
                b.SetCount++;
            }
            return isSetWinner;
        }
        //Determina si jugador gana el partido basandose en limite de sets
        public bool MatchWinner(Gamer a, Gamer b, TennisGame ng)
        {
            bool hayGanador = false;

                if (ng.SetLimit == 3)
                {
                    if(a.SetCount >= 2)
                    {
                        hayGanador = true;
                    ng.Ganador = a.GamerName;
                    }
                    else if(b.SetCount >= 2)
                    {
                        hayGanador = true;
                    ng.Ganador = b.GamerName;
                }
                }
                else
                {
                    if(a.SetCount >= 3)
                    {
                        hayGanador = true;
                    ng.Ganador = a.GamerName;
                }
                    else if (b.SetCount >= 3)
                    {
                        hayGanador = true;
                    ng.Ganador = b.GamerName;
                }
                }
            return hayGanador;
        }
        //Resetea el contador de juegos de jugadores
        public void ResetGames(Gamer a, Gamer b)
        {
            a.WinnedGames = 0;
            b.WinnedGames = 0;   
        }
        //Resetea la puntuacion de los jugadores
        public void ResetPoints(Gamer a, Gamer b)
        {
            a.Points = 0;
            b.Points = 0;
            a.Score = "0";
            b.Score = "0";   
        }
        //Determina cual de los jugadores puntua basandose en metodo random
        public string ShootTime(Gamer a, Gamer b)
        {
            string name = "";
            if (a.ThrowBall())
            {   
                a.Points++;
                name= a.GamerName;
            }
            else
            {
                b.Points++;
                name= b.GamerName;
            }
            return name;
        }
        //Actualiza la puntuacion global y asigna puntuacion a los sets
        public void CalcGameScore(Gamer a, Gamer b, TennisGame ng)
        {
            DrawTab(a, b, ng);
            ResetPoints(a, b);
            switch (ng.set)
            {
                case 1:
                    a.Set1 = a.WinnedGames;
                    b.Set1 = b.WinnedGames;
                    break;
                case 2:
                    a.Set2 = a.WinnedGames;
                    b.Set2 = b.WinnedGames;
                    break;
                case 3:
                    a.Set3 = a.WinnedGames;
                    b.Set3 = b.WinnedGames;
                    break;
                case 4:
                    a.Set4 = a.WinnedGames;
                    b.Set4 = b.WinnedGames;
                    break;
                case 5:
                    a.Set5 = a.WinnedGames;
                    b.Set5 = b.WinnedGames;
                    break;
            }
        }
        //Determina limite de sets
        public int SetLimit()
        {
            bool answer = false;
            int setsLimit = 0;
            string msg = "Game will be 3 or 5 sets ?: ";

            do
            {
                Console.WriteLine(msg);
                answer = Int32.TryParse(Console.ReadLine(), out setsLimit);

                if (setsLimit != 3 && setsLimit != 5)
                {
                    answer = false;
                }

            } while (!answer);

            return setsLimit;
        }
        //Guarda formato en string y actualiza valores de puntuacion
        public void DrawTab(Gamer a, Gamer b, TennisGame ng)
        {
            if (ng.SetLimit == 5)
            {
                ng.table = String.Format($"({a.Set1}-{b.Set1}, {a.Set2}-{b.Set2}, {a.Set3}-{b.Set3}, {a.Set4}-{b.Set4}, {a.Set5}-{b.Set5})");

            }
            else
            {
                ng.table = String.Format($"({a.Set1}-{b.Set1}, {a.Set2}-{b.Set2}, {a.Set3}-{b.Set3})");
            }  
        }

    }
}
