﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestTennisGame
{
    [TestClass]
    public class UnitTest1
    {
        SimulatorTennisGame.TennisRules tennis = new SimulatorTennisGame.TennisRules();
        SimulatorTennisGame.Gamer g1 = new SimulatorTennisGame.Gamer("Player1");
        SimulatorTennisGame.Gamer g2 = new SimulatorTennisGame.Gamer("Player2");


        [TestMethod]
        public void DeterminarGanador()
        {
           
            string winner = tennis.ShootTime(g1, g2);
            bool todoOk = false;

            if (winner.Equals(g1.GamerName) || winner.Equals(g2.GamerName))
            {
                todoOk = true;
            }
          
            Assert.AreEqual(todoOk, true);
        }

        [TestMethod]
        public void CalcularResultado()
        {
            g1.Points = 4;
            g2.Points = 3;

            tennis.CalcScore(g1, g2);
         
            Assert.AreEqual(g1.Score, "AD");
        }



    }
}
